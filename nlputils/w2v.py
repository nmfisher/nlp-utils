import keras
from keras.layers.wrappers import Bidirectional
from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Reshape, Merge, Input,RepeatVector,Lambda
from keras.layers.merge import Concatenate, dot, concatenate,average
from keras.layers.recurrent import LSTM
from keras.optimizers import RMSprop
from keras.callbacks import ModelCheckpoint
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras import backend as K
    
class Word2Vec:
    def __init__(self, lookup, embedding_size, learning_rate, decay):
        self.embedding = Embedding(len(lookup), embedding_size)

        self.target = Input(shape=(1,), dtype='int32')
        self.target_e = self.embedding(self.target)
        self.target_e = Reshape((1,embedding_size))(self.target_e)

        self.context = Input(shape=(1,), dtype='int32')
        self.context_e = self.embedding(self.context)
        self.context_e = Reshape((1,embedding_size))(self.context_e)

        self.concat = Reshape((1,))(dot([self.target_e,self.context_e],2))

        self.predictions = Activation('sigmoid')(self.concat)

        self.inputs = [self.target,self.context]

        self.model = Model(inputs=self.inputs, outputs=[self.predictions])
        rms = keras.optimizers.RMSprop(lr=learning_rate, rho=0.9, epsilon=1e-08, decay=decay)
        self.model.compile(loss='binary_crossentropy', optimizer=rms)

        self.prob_fn = K.function([self.model.layers[0].input,self.model.layers[1].input],[self.model.layers[-1].output])
        self.embedding_fn = K.function([self.model.layers[0].input],[self.model.layers[2].get_output_at(0)])