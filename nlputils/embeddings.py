import re
import numpy as np
from scipy.spatial.distance import euclidean

class Embeddings:
    def __init__(self, path, fmat="w2v"):
        if fmat == "w2v":
            self.rgx = re.compile(" |\t|\n")
        elif fmat == "fasttext":
            self.rgx = re.compile(" |\t|\n");
        else:
            raise Exception("Unknown format")
        self.parse_rgx = re.compile(" |\.|,|\"|\“|\”|\(|\)")
        rgx = self.rgx
        with open(path, "r",encoding="utf8") as infile:
            w2i = {}
            embeddings = []
            i = 0;
            for line in infile:
                if i == 0 and fmat == "fasttext":
                    i+=1
                    continue
                split = rgx.split(line)
                split = [s for s in split if len(s) > 0]
                word = split[0]
                row = np.array(split[1:]).astype(np.float)

                i += 1
                if len(embeddings) > 0 and row.shape != embeddings[0].shape:
                    print("Error at line number: {0}, skipping".format(i))
                else:
                    embeddings.append(row)
                    if word not in w2i:
                        w2i[word] = len(w2i)

        self.embedding_dim = embeddings[0].shape[0]
        if "<s>" not in w2i:
            w2i["<s>"] = len(w2i)
            embeddings.append(np.random.normal(0,0.1,embeddings[0].shape[0]))
        if "</s>" not in w2i:
            w2i["</s>"] = len(w2i)
            embeddings.append(np.random.normal(0,0.1,embeddings[0].shape[0]))
        self.matrix = np.matrix(embeddings)
        self.lookup = w2i
        print("Created embeddings matrix of shape : {0}".format(self.matrix.shape))
        
    
    def __getitem__(self, token):
        return self.matrix[self.lookup[token]]
        
         
    def tokenize_string(self, string):
        returned = 0;
        for t in self.parse_rgx.split(string):
            if t != "" and t in self.lookup:
                yield t
                returned += 1
    
    def tokenize(self, path):
        with open(path,"r",errors="replace") as file:
            return self.tokenize_string(file.read())
            
    def vectorize_string(self, string):
        text = list(self.tokenize_string(string))
        vectors = []
        for token in text:
            if token != "" and token in self.lookup:
                vectors.append(self[token])
        return vectors, text
            
    def pad(self, vectors, max_tokens):              
        if len(vectors) == 0:
            vectors = [np.zeros((1,self.embedding_dim))]
        while len(vectors) < max_tokens:
            vectors.append(np.mean(vectors,axis=0))
        return vectors;
    
    def vectorize(self, path):
        with open(path,"r",errors="replace") as file:
            doc = file.read()
            return self.vectorize_string(doc)
            
    def nearest_neighbours(self, vector, nn=5, tol=0.001):
        dists = np.asarray(np.dot(vector, self.matrix.T))[0]       
        return np.argsort(dists)[:nn]
        
