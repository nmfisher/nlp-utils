import numpy as np
import random
class CBOW:
    def __init__(self, nlp, docs, lookup, window_size=2, negative_samples=1):
        self.nlp = nlp
        self.lookup = lookup
        self.window_size = window_size
        print(self.window_size)
        self.negative_samples = negative_samples
        self.docs = docs
        self.inputs = [[],[]]
        self.labels = []
        self.generate_cbow()
        
    def extractTokens(self, doc):
        return [self.lookup[t.text.lower()] for t in self.nlp(doc)]           
        
    def generate_cbow(self):
        print("Starting tokenization..")
        for doc in self.docs: 
            tokens = self.extractTokens(doc)
            doc_inputs, doc_labels = self.tokens_to_cbow(tokens, self.window_size)
            if doc_inputs is None:
                print("Skipping empty document")
                continue
            doc_inputs, doc_labels = self.shuffle(doc_inputs, doc_labels)
            self.inputs[0] += doc_inputs[0]
            self.inputs[1] += doc_inputs[1]
            self.labels += doc_labels
            print("Finished tokenizing document {0}".format(str(self.docs.index(doc))))
        print("Tokenization complete..")

    def shuffle(self, inputs, labels):
        shuffled_inputs = [[],[]]
        shuffled_labels = []
        for i in np.random.choice(np.arange(len(inputs[0])), size=len(inputs[0]), replace=False):
            shuffled_inputs[0].append(inputs[0][i])
            shuffled_inputs[1].append(inputs[1][i])
            shuffled_labels.append(labels[i])
        return shuffled_inputs, shuffled_labels
        
    def tokens_to_cbow(self, tokens, window_size):
        target_contexts = []
        sm = []
        sp = []
        labels = []
        vocab = list(self.lookup.keys())
        for t in range(len(tokens) - window_size):
            for i in range(window_size):
                labels.append(1)
                target_contexts.append((tokens[t], tokens[t+i+1]))
                labels.append(1)
                target_contexts.append((tokens[t], tokens[t-i-1]))
                for i in range(self.negative_samples*2):
                    target_contexts.append((tokens[t], random.randint(0, len(self.lookup)-1)))
                    labels.append(0)
        
        if target_contexts:
            word_target, word_context = zip(*target_contexts)
            word_target = np.array(word_target, dtype="int32")
            word_context = np.array(word_context, dtype="int32")
            inputs = [word_target, word_context]
            return inputs, labels
        return None,None
        
    def batch_generator(self, batch_size):
        for i in range(1, int(len(self.inputs[0]) / batch_size)):
            batch_inputs = [np.array(self.inputs[0][(i-1)*batch_size:i*batch_size]), np.array(self.inputs[1][(i-1)*batch_size:i*batch_size])]
            batch_labels = np.array(self.labels[(i-1)*batch_size:i*batch_size])
            yield batch_inputs, batch_labels