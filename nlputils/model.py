import h5py
import numpy as np
import spacy

class ModelWrapper:
    def __init__(self, nlp, model, cbow, lookup, epochs=10, batch_size=64):
        self.nlp = nlp;
        self.model = model;
        self.cbow = cbow;
        self.epochs = epochs;
        self.batch_size = batch_size;
        self.losses = []
        self.lookup = lookup;
       
    def train(self):
        self.losses = [];
        for j in range(self.epochs):                   
            batch_gen = self.cbow.batch_generator(self.batch_size);
            for batch_inputs, batch_labels in batch_gen:
                loss = self.model.model.train_on_batch(batch_inputs,batch_labels)
                if len(self.losses) > 1000:
                    print(np.mean(self.losses))
                    self.losses = self.losses[500:]
                self.losses.append(loss)
            print("Epoch {0} complete".format(str(j)))
            #print("Train perplexity : {0}".format(str(self.perplexity([self.cbow.docs[0]]))))
            
    def isSequenceEnd(self, token, accum):
        return len(accum) > 1 and (token.text == "\n" or token.text == "\r\n")
        
    def cbowProbs(self, accum, i):
        probs = []
        for j in range(0, self.cbow.window_size):
            word_target = np.reshape(np.array([self.lookup[accum[i]]],dtype="int32"),(1,1))
            if i - j > 0:
                word_context_left = np.reshape(np.array([self.lookup[accum[i-j-1]]],dtype="int32"),(1,1))
                probs.append(self.model.prob_fn([word_target,word_context_left]))
            if i + j < len(accum) - 1:
                word_context_right = np.reshape(np.array([self.lookup[accum[i+j+1]]],dtype="int32"),(1,1))
                probs.append(self.model.prob_fn([word_target,word_context_right]))
        return np.nanmean(probs)
        
    def get_probabilities(self, docs):
        probabilities = []
        scored_tokens = []
        def raw_tokens():
            for doc in docs:
                for token in self.nlp(doc):
                    yield token
                
        accum = []

        for token in raw_tokens():    
            if self.isSequenceEnd(token, accum):
                for i in range(len(accum)):
                    probabilities.append(self.cbowProbs(accum, i))
                    scored_tokens.append(accum[i])
                accum = []
            else:
                accum.append(token.text.lower())    
        return probabilities, scored_tokens
    
    def predict(self, docs, labels, labels_partial):
        probabilities, scored_tokens = self.get_probabilities(docs)
        return self.get_labels(probabilities, scored_tokens, labels, labels_partial)
                
                
    def get_labels(self, probabilities, scored_tokens, labels, labels_partial, threshold = 0.5):
        full_tokens = {}
        actual_partial = []
        predictions_partial = []
        predictions = {}
        
        for i in range(len(probabilities)):
            if probabilities[i] < threshold:
                predictions_partial.append(1)
            else:
                predictions_partial.append(0)    
            
            sub_token = scored_tokens[i]
            
            if sub_token in labels_partial:
                actual_partial.append(1)
            else:
                actual_partial.append(0)
            
            if sub_token in labels_partial:
                for full_token in labels_partial[sub_token]:
                    j = 1
                    while sub_token in full_token and i+j < len(scored_tokens):
                        sub_token = sub_token + " " + scored_tokens[i+j]
                        if sub_token in labels:
                            full_tokens[sub_token] = True
                            if np.mean(probabilities[i:j]) < threshold:
                                predictions[sub_token] = True;     
                        j+= 1
        return full_tokens, actual_partial, predictions_partial, predictions
        
    def generate_embeddings(self):
        self.embeddings = np.zeros((len(self.lookup),self.embedding_size))
        i = 0
        for word in lookup:
            self.embeddings[i] = self.model.embedding_fn([np.reshape(np.array([self.lookup[word]]),(1,1))])[0]
            i += 1
        
    def save_embeddings(self, filepath):
        self.generate_embeddings()
        h5f = h5py.File(filepath, 'w')
        h5f.create_dataset('word', data=self.embeddings)
        h5f.close()
        
    def perplexity(self, docs):
        probabilities, scored_tokens = self.get_probabilities(docs)
        return np.exp(-sum([p * np.log(p) for p in probabilities if np.isnan(p) == False]) / len(probabilities))