import os
import spacy
import json
import numpy as np
import random 
from sklearn import model_selection

from collections import defaultdict

def walk(dir, ext=None, topLevelOnly=False):
    files = []
    labels = []
    dirSplit = dir.split(os.path.sep)       

    for root, dirs, dirfiles in os.walk(dir):
        subSplit = root.split(os.path.sep)       
        for filename in [f for f in dirfiles if f.endswith(ext)]:
            if topLevelOnly:
                labels.append(subSplit[len(dirSplit)])
            else:
                labels.append(subSplit[len(subSplit) - 1])
            files.append(os.path.join(root, filename))
    return files, labels
    
def load(docDir, suffix, labelFile):
    """ recurse through the given directory and return, for each file found matching suffix:
    - a list of tuples containing (0) the filename and (2) the text content of each file
    - a dict of labels from labelFile (a file containing one label per line)
    - a dict of partial labels """
    docs = loadDocuments(docDir, suffix)
    labels, labels_partial = loadLabels(labelFile)
    return docs, labels, labels_partial
    
def load_train_test_validation(docDir, suffix, labelFile, test_size=0.05, validation_size=0.05):
    """ return three tuples containing train docs/labels, test docs/labels, val docs/labels"""
    docs, labels, labels_partial = load(docDir, suffix, labelFile);
    docs_train, docs_test = model_selection.train_test_split(docs, test_size=test_size + validation_size)
    docs_test, docs_val = model_selection.train_test_split(docs_test, test_size=validation_size / (test_size + validation_size))
    return (docs_train, docs_test, docs_val), labels, labels_partial

def loadDocuments(dir, suffix):
    docs = []
    filenames = []
    for filename in os.listdir(dir):
        filepath = os.path.join(dir, filename)
        if os.path.isfile(filepath) and filename.endswith(suffix): 
            filenames.append(filename)
            with open(filepath,'r') as file:
                docs.append(file.read())
    return list(zip(docs, filenames))
    
def loadLabels(filepath):
    labels = {}
    with open(filepath,"r") as infile:
        for line in infile.readlines():
            line = line.replace("\"","").replace("\n","")
            labels[line] = True
    labels_partial = defaultdict(list)
    for label in labels.keys():
        for s in label.split(" "):
            if len(s) > 0:
                labels_partial[s].append(label)                
    return labels, labels_partial

    

def get_confidence_color(confidence):
    if confidence < 0.20:
        return "\x1b[38;5;196m"
    elif confidence < 0.40:
        return "\x1b[38;5;161m"
    elif confidence < 0.60:
        return "\x1b[38;5;126m"
    elif confidence < 0.70:
        return "\x1b[38;5;91m"
    elif confidence < 0.80:
        return "\x1b[38;5;56m"
    else:
        return "\x1b[38;5;21m"
