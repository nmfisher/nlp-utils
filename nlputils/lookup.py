from collections import defaultdict

class Lookup:
    def __init__(self, nlp, docs, dictDumpFile=None, save=False):
        self.nlp = nlp
        self.lookup = {}
        self.freqs = defaultdict(int)
        self.num_tokens = 0
        for doc in docs:
            for token in self.nlp(doc):
                token = token.text.lower()
                if token not in self.lookup:
                    self.lookup[token] = len(self.lookup)
                self.freqs[token] += 1
                self.num_tokens += 1
        if save and dictDumpFile:
            with open(dictDumpFile, 'w') as outfile:
                json.dump(lookup, outfile)
                
    def __getitem__(self, key):
        return self.lookup[key]

    def __setitem__(self, key, value):
        self.lookup[key] = value
        
    def keys(self):
        return self.lookup.keys()
        
    def __len__(self):
        return len(self.lookup)